import log from "@ajar/marker";
import { saySomething } from "./myModule.js";

const response = saySomething("hello");
(x: number) => console.log(x);
log.magenta(response);
